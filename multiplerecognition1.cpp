#include <boost/filesystem.hpp>
//#include <opencv2/opencv.hpp>
#include <boost/gil/gil_all.hpp>
#include <boost/gil/extension/io/png_io.hpp>

using namespace std;

int main()
{
	int i = 0;
	char command[256];
	vector<string> allPNGFileNames;
	FILE* fi;
	boost::gil::rgb8_image_t path;
	namespace fs = boost::filesystem;
	fs::directory_iterator end;

	for (fs::directory_iterator it("/home/asaba/unifiedparsing-master/20170517131907/"); it != end; ++it)
	{
		if (fs::is_regular_file(*it) && fs::extension(*it) == ".png")
			allPNGFileNames.push_back(it->path().string());

		//const fs::path path(it->path());
		//const fs::path dest("/home/asaba/unifiedparsing-master/test.png");
		//fs::copy_file(path, dest, fs::copy_option::overwrite_if_exists);
		//cv::Mat image = cv::imread(it->path().string());
		//cv::imwrite("/home/asaba/unifiedparsing-master/test.png", image);
		boost::gil::png_read_image(it->path(), path);
		boost::gil::png_write_view("/home/asaba/unifiedparsing-master/test.png", path);
		system("/home/asaba/multiplerecognition1/multiplerecognition1/more_test.txt");
		sprintf(command, "/home/asaba/unifiedparsing-master/result%d.txt", i);
		fi=fopen(command, "w");
		const fs::path path1("/home/asaba/unifiedparsing-master/material_label_map.txt");
		const fs::path dest1("fi");
		fs::copy_file(path1, dest1, fs::copy_option::overwrite_if_exists);
		fclose(fi);
		++i;
	}
}